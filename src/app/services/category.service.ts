import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../models/category';

@Injectable({ providedIn: 'root' })
export class CategoryService {

    constructor(private http: HttpClient) { }

    createEditCategory(category: Category): Observable<number> {
        const action = category.id === 0 ? 'Create' : 'Edit';
        return this.http.post<number>(`https://localhost:5001/Category/${action}`, category);
    }

    getCategory(id: string): Observable<Category> {
        return this.http.get<Category>(`https://localhost:5001/Category/Edit/${id}`)
    }

    getCategories(): Observable<Category[]> { 
        return this.http.get<{ categories: Category[] }>('https://localhost:5001/Category/List')
        .pipe(map(response => response.categories));
    } 
}