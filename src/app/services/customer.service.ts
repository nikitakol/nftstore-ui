import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Customer } from '../models/customer';

@Injectable({ providedIn: 'root' })
export class CustomerService {

    constructor(private http: HttpClient) { }

    createEditCustomer(customer: Customer): Observable<number> {
        const action = customer.id === 0 ? 'Create' : 'Edit';
        return this.http.post<number>(`https://localhost:5001/Customer/${action}`, customer);
    } 

    getCustomer(id: string): Observable<Customer> {
        return this.http.get<Customer>(`https://localhost:5001/Customer/Edit/${id}`)
    }

    getCustomers(): Observable<Customer[]> { 
        return this.http.get<{ customers: Customer[] }>('https://localhost:5001/Customer/List')
        .pipe(map(response => response.customers));
    } 
}