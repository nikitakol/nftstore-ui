export class Category {
    id: number;
    name: string;
    enabled: boolean;
    modifiedBy: string;
    modifiedOn: Date;
}