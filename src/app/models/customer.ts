import { Address } from "./address";

export class Customer {
    id: number;
    firstName: string;
    middleInitial: string;
    lastName: string;
    email: string;
    shippingAddress: Address;
    billingAddress: Address;
    modifiedBy: string;
    modifiedOn: Date;
}