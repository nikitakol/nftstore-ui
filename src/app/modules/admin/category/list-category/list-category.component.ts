import { Component, OnInit, OnDestroy } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { Category } from 'src/app/models/category';
import {MatGridListModule} from '@angular/material/grid-list';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {
  private destroy$ = new Subject();
  categories: Category[];
  displayedColumns: string[] = ['name', 'enabled', 'modifiedOn', 'modifiedBy', 'actions']

  constructor(private service: CategoryService) { }

  ngOnInit(): void {
    this.service.getCategories().subscribe(result => {
      this.categories = result;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
