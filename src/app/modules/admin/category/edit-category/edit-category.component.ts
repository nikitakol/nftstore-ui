import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();
  categoryForm: FormGroup;
  category = new Category();
  enabled: boolean;

  constructor(
    private service: CategoryService, 
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute) {
   }

  ngOnInit(): void {
    this.categoryForm = this.formBuilder.group({
      name: [null, Validators.required]
    });

    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id === undefined) {
        return;
      }

      this.service.getCategory(id)
        .pipe(takeUntil(this.destroy$))
        .subscribe(category => {
          this.setView(category);
        });
    });
  }

  setView(category: Category): void {
    this.category = category;
    
    this.categoryForm.controls.name.setValue(this.category.name);
    this.enabled = this.category.enabled;
  }   

  onSubmit(): void {
    if (!this.categoryForm.valid) {
      return;
    }

    Object.assign(this.category, this.categoryForm.value);
    this.category.enabled = this.enabled;

    this.service.createEditCategory(this.category)
      .pipe(takeUntil(this.destroy$))
      .subscribe(id => {
        this.router.navigate(['admin', 'list-category'])
      });
  }

  OnCheckboxChanged(event: any): void 
  { 
    this.enabled = false;

    if(event.checked) {
      this.enabled = true;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
