import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Address } from 'src/app/models/address';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.css']
})
export class ListCustomerComponent implements OnInit {
  private destroy$ = new Subject();
  customers: Customer[];
  displayedColumns: string[] = ['firstName', 'middleInitial', 'lastName', 'email', 
  'shippingAddress', 'billingAddress', 'modifiedOn', 'modifiedBy']

  constructor(private service: CustomerService) { }

  ngOnInit(): void {
    this.service.getCustomers().subscribe(result => {
      this.customers = result;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
