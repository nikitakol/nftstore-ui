import { ListCategoryComponent } from "./category/list-category/list-category.component";
import { EditCategoryComponent } from "./category/edit-category/edit-category.component";
import { ListCustomerComponent } from "./customer/list-customer/list-customer.component";

export const routes = [
    {
        path: 'list-category',
        data: { title: 'Categories' },
        children: [
            { path: '', component: ListCategoryComponent }
        ]
    },

    {
        path: 'edit-category',
        data: { title: 'Category' },
        children: [
            { path: '', component: EditCategoryComponent}
        ]
    },

    {
        path: 'edit-category/:id',
        data: { title: 'Category' },
        children: [
            { path: '', component: EditCategoryComponent}
        ]
    },

    {
        path: 'list-customer',
        data: { title: 'Customer' },
        children: [
            { path: '', component: ListCustomerComponent }
        ]
    }
]