import { routes } from './admin.routes';
import { RouterModule } from '@angular/router';
import { NgModule } from "@angular/core";
import { ListCategoryComponent } from "./category/list-category/list-category.component";
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListCustomerComponent } from './customer/list-customer/list-customer.component';

@NgModule({
    declarations: [
        ListCategoryComponent,
        EditCategoryComponent,
        ListCustomerComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatTableModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class AdminModule { }